import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from 'src/app/services/heroes.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html'
})
export class BuscarComponent implements OnInit {
  heroes: Heroe[];
  termino: string;
  back: string;

  constructor(
    private activateRouter: ActivatedRoute,
    private _heroesService: HeroesService,
    private router: Router) { }

  ngOnInit() {
    this.activateRouter.params.subscribe(params => {
      this.termino = params.termino;
      this.back = `/buscar/${ this.termino }`;
      this.heroes = this._heroesService.buscarHeroe(this.termino);
    })
  }

  verHeroe(i) {
    this.router.navigate(['heroe', i, this.back]);
  }

}
